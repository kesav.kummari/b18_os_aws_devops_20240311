#### Getting Started with AWS DevOps

# Session Video
```
https://drive.google.com/file/d/1LMrULfLCe1GSJYU5zeCClMmM8MZqiajq/view?usp=sharing
```

#### Windows Basics

```
Here are some commonly used Windows Command Prompt (CMD) commands:

cd (Change Directory):

cd directory_path: Change the current working directory.
dir (Directory Listing):

dir: List the files and directories in the current directory.
mkdir (Make Directory):


This creates a file named "filename.txt" in the current directory.
echo "Welcome To Cloud Binary" > filename.txt

This also creates an empty file named "filename.txt" in the current directory.
type nul > filename.txt

To Read a file:
type filename.txt

mkdir directory_name: Create a new directory.
rmdir (Remove Directory):

rmdir /s /q directory_name: Remove a directory and its contents.
copy:

copy source_file destination: Copy files from one location to another.
move:

move source destination: Move files from one location to another.
del (Delete):

del file_name: Delete a file.
ren (Rename):

ren old_name new_name: Rename a file or directory.
type:

type file_name: Display the contents of a text file.
echo:

echo text: Display text on the screen.
cls (Clear Screen):

cls: Clear the command prompt window.
ipconfig:

ipconfig: Display network configuration information.
ping:

ping destination: Test network connectivity to a specific host.
netstat:

netstat: Display network statistics and active connections.
tasklist:

tasklist: Display a list of running processes.
taskkill:

taskkill /F /IM process_name: Terminate a running process forcefully.
systeminfo:

systeminfo: Display detailed configuration information about the computer.
sfc (System File Checker):

sfc /scannow: Scan and repair system files.
chkdsk (Check Disk):

chkdsk /f: Check and repair disk errors.
shutdown:

shutdown /s: Shutdown the computer.


These are just a few examples, and there are many more commands available in the Windows Command Prompt. You can use the help command followed by a specific command to get more information about its usage. 

For instance, help cd will provide details about the cd command.


sc (Service Control):

sc query: Lists all services on the system.
sc query service_name: Displays detailed information about a specific service.
sc queryex service_name: Provides extended information about a service.

Example:
    sc query wuauserv


net (Network):

net start: Lists all running services.
net stop service_name: Stops a running service.

Example:
    net start

tasklist:

tasklist /svc: Displays a list of all running processes and their associated services.

Example:
    tasklist /svc

ervices.msc (Services GUI):

services.msc: Opens the Services management console, providing a graphical interface to view and manage services.

Example:
    services.msc

wmic (Windows Management Instrumentation Command-line):

wmic service list brief: Lists all services and their basic information.
wmic service where "name='service_name'" get caption, startname, state: Retrieves specific information about a service.

Example:
    wmic service list brief


net user:

net user: Lists all user accounts on the system.
net user username password /add: Adds a new user account.
net user username /delete: Deletes a user account.
net user username new_password: Changes the password for a user account.
net user username /active:yes: Activates a user account.
net user username /active:no: Deactivates a user account.

Examples:
net user                # List all user accounts
net user John password /add   # Add a new user named John with the password 'password'
net user Jane /delete   # Delete the user account named Jane
net user Bob newpass    # Change the password for the user Bob to 'newpass'


net localgroup:

net localgroup: Lists all local groups on the system.
net localgroup groupname username /add: Adds a user to a local group.
net localgroup groupname username /delete: Removes a user from a local group.

Examples:
net localgroup            # List all local groups
net localgroup Administrators John /add  # Add user John to the Administrators group
net localgroup Users Jane /delete         # Remove user Jane from the Users group

wmic useraccount:

wmic useraccount: Lists detailed information about user accounts.

Example:
wmic useraccount get name,sid  # Get names and SIDs of all user accounts


To terminate or kill a job or process in the Windows Command Prompt, you can use the taskkill command. Here's how you can do it:

taskkill /F /PID process_id


Open the Command Prompt.

Use the tasklist command to display a list of running processes and find the Process ID (PID) of the job you want to kill.

tasklist


Once you have identified the PID, use the taskkill command:
taskkill /F /PID 1234




```