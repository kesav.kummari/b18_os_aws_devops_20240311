#### Getting Started with AWS DevOps

# Session Video
```
https://drive.google.com/file/d/1p6KO6sPmdFm2nj82gSZjgJizoK7t7CxG/view?usp=sharing
```

#### Agenda :

```
1. AWS IAM : Global Service 

2. Amazon EC2 : Region base Service

```
#### 

- To attach permissions for Load Balancers and Auto Scaling Groups along with the existing permissions for EC2, you need to modify the policy to include actions related to these services as well. Here's how you can update the policy:

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ec2:*",
                "elasticloadbalancing:*",
                "autoscaling:*"
            ],
            "Resource": "*",
            "Condition": {
                "StringEquals": {
                    "aws:RequestedRegion": "us-east-1"
                }
            }
        }
    ]
}

```

####

```

Creating Policy with the Updated JSON
Go to the AWS Management Console and open the IAM console.
In the navigation pane, choose "Policies", then click on "Create policy".
Choose the "JSON" tab and paste the updated policy JSON.
Click on "Review policy".
Name your policy (e.g., "EC2_ELB_ASG_Access_US-East-1") and add a description.
Click on "Create policy".
Attaching the Updated Policy to the User
Once you've created the updated policy, follow the steps to attach it to the user:

In the IAM console, click on "Users" in the navigation pane, then click on the user you've created earlier (e.g., "EC2LimitedUser").
Click on the "Add permissions" button.
Choose "Attach existing policies directly".
Search for the updated policy you just created (e.g., "EC2_ELB_ASG_Access_US-East-1") and select it.
Click on "Next: Review".
Review the permissions and click on "Add permissions".
Now, the user has permissions to manage EC2 instances, Elastic Load Balancers, and Auto Scaling Groups in the us-east-1 region.
```